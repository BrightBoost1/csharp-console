﻿namespace LearnCSharpApp
{
    internal class Program
    {
        static void DisplayWelcome(string name)
        {
            Console.WriteLine("Welcome " + name);
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, what's your name?");
            string name = Console.ReadLine();
            DisplayWelcome(name);
            long x = 12;
            float y = 1.0f;
            double z = 2.0;
        }
    }
}